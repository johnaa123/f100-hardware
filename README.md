# Description

This is an open-source replacement PCB for the Odroid-GO <https://wiki.odroid.com/odroid_go/odroid_go>

# Specifications

- 400MHz STM32H7 MCU
- 32MiB 16-bit 133MHz SDRAM (Upgradable to 64MiB, potentially overclockable to 200MHz)
- 320x240 18-bit 60Hz 2.4" LCD (Hardware dithering to 24-bit, may support 120Hz refresh)
- 12-bit Audio Output with hardware volume control
- 4-bit microSD Card Interface (50MHz supported, potentially overclock to 100MHz)
- PWM-RGB Status LED

# Tools

You'll need KiCad 5 to view or modify the schematics/pcb.
For Ubuntu users I'd suggest using this PPA: <https://launchpad.net/~js-reynaud/+archive/ubuntu/kicad-5>

Initial routing for this board was done using FreeRouting: <https://freerouting.org>
